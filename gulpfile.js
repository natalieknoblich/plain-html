var gulp = require("gulp");
var browserSync = require("browser-sync").create();
var sass = require("gulp-sass");
var minify = require("gulp-minify");

gulp.task("serve", ["sass", "js"], function () {
    browserSync.init({
        server: {
            baseDir: "./public",
        },
    });

    gulp.watch("public/scss/**/*.scss", ["sass"]);
    gulp.watch("public/js/**/*.js", ["js-watch"]);
    gulp.watch(["public/index.html"]).on("change", browserSync.reload);
});

gulp.task("sass", function () {
    return gulp.src("public/scss/*.scss")
        .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(gulp.dest("public/css"))
        .pipe(browserSync.stream());
});

gulp.task("js", function () {
    return gulp.src("public/js/**/*.js")
        .pipe(minify({noSource: true }))
        .pipe(gulp.dest("public/js"));
});

gulp.task("js-watch", ["js"], function(done) {
    browserSync.reload();
    done();
});

gulp.task("build", ["sass", "js"]);

gulp.task("default", ["serve"]);