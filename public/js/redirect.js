if (location.protocol != 'https:') {
    var url = window.location.href.substring(window.location.protocol.length);
    if (url.indexOf("localhost") === -1) {
        location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
    }
}