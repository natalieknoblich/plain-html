var nameInput = document.getElementById("nameInput");
var emailInput = document.getElementById("emailInput");
var messageInput = document.getElementById("messageInput");
var honeypot = document.getElementById("phoneInput");
var sendButton = document.getElementById("sendButton");

var contactWrapper = document.getElementById("contact");
var messageToUser = document.getElementById("message-to-user");

var inputElements = ["input", "textarea", "button"];

function error(message) {
    messageToUser.innerText = message;
    messageToUser.className = "error-message";
}

function info(message) {
    messageToUser.innerText = message;
    messageToUser.className = "info-message";
}

function success(message) {
    messageToUser.innerText = message;
    messageToUser.className = "success-message";
}

function disableInputs() {
    inputElements.forEach(function (el) {
        $(".contact " + el).prop("disabled", true);
    });
}

function enableInputs() {
    inputElements.forEach(function (el) {
        $(".contact " + el).prop("disabled", false);
    });
}

function sendEmail(name, email, message) {
    disableInputs();
    info("Sending your message now...");

    var url = "https://script.google.com/macros/s/AKfycbzdHoZsi4K-VsMucLCBpYjrTyzJITLvOB-A40SItXFT2BOCSeSa/exec";

    var data = new FormData();
    data.append("name", name);
    data.append("email", email);
    data.append("message", message);

    $.ajax({
        url: url,
        data: data,
        processData: false,
        contentType: false,
        type: "POST",
        success: function (data) {
            success("Message sent!");
        },
        error: function () {
            error("Sorry, something went wrong sending your message");
            enableInputs();
        }
    });
}

function processEmailInput() {
    var name, email, message;

    if (honeypot.value !== "") {
        error("Bot warning triggered, message not sent.");
        return;
    }

    name = nameInput.value.trim();
    if (name === "") {
        error("Please enter a name before sending your message.");
        return;
    }

    email = emailInput.value.trim();
    if (email === "") {
        error("Please enter an email before sending your message.");
        return;
    }

    if (email.indexOf("@") === -1) {
        error("Please enter a valid email address.");
        return;
    }

    message = messageInput.value.trim();
    if (message === "") {
        error("Please enter a message to send.");
        return;
    }

    sendEmail(name, email, message);
}

sendButton.addEventListener("click", processEmailInput);